_Robozzle_
==========

In the robozzle game, you program a robot to walk a map and fetch all the stars.
To program the robot, you put commands into slots, each list of slot is a function (F1, F2, F3...),
and a command is composed of an action and a color.

The list of available actions are :
 - go forward 1 case
 - rotate 90deg left
 - rotate 90deg right
 - call a function (F1, F2, F3...)

A color means the action will be executed only if the robot is on a case whose color matches.
For example, a command "go-fwd red" means "go fwd only if the robot is on a red case"
If a slot has no color (blank slot), the command will always execute.
The list of colors are :
- red
- blue
- green
- yellow
- no color

Play online
-----------

An online version of the game is available at :
https://nicolas_van_phan.gitlab.io/robozzle/

How to build the project
------------------------

Run `make` to compile the libraries and executables that are
meant to be installed.
```
$ make
```
Then launch the server to access the game at <http://localhost:8080> :
```
$ ./app/webserver.exe
```
*Note :* You can also directly open the HTML page of the game locally :
However, doing do requires disabling the CORS feature on your browser to enable loading of the level and pictures.
On Firefox, you can disable it in `about:config` by setting `strict_origin_policy` to false.
```
./app/index.html
```