(*
   Tests for the Adj library
*)
open Lib_adj

let check msg x = Alcotest.(check bool) msg true x

type color = Green | Blue | Yellow | Red | Empty

let val1_map =
  [
    (0, [  (4, Blue); (6, Red);  ] );
    (1, [  (4, Blue); (5, Blue );  ] );
    (2, [  (4, Blue); (4, Blue );  ] );
    (3, [  (3, Blue); (4, Blue );  ] );
    (* (4, [  (2, Blue); (4, Blue );  ] ); *)
    (5, [  (1, Blue); (4, Blue );  ] );
    (6, [  (0, Blue); (4, Blue );  ] );
    (7, [  (0, Green)               ] );
  ]

let test_mem () =
  check "There's something at index 0" ((Adj.mem 0 val1_map) == true);
  check "There's something at index 4" ((Adj.mem 4 val1_map) == false);
  check "There's something at index 7" ((Adj.mem 7 val1_map) == true);
  check "There's nothing at index 8"   ((Adj.mem 8 val1_map) == false)

let map1_exists index = match (Adj.get index val1_map) with
  | Some _ -> true
  | None -> false

let test_get () =
  check "There's something at index 0" ((map1_exists 0) == true);
  check "There's something at index 3" ((map1_exists 3) == true);
  check "There's nothing   at index 4" ((map1_exists 4) == false);
  check "There's something at index 7" ((map1_exists 7) == true);
  check "There's nothing   at index 8" ((map1_exists 8) == false)

let test_mem_matrix () =
  check "Get element" ((Adj.mem_matrix (0, 0) val1_map) == false);
  check "Get element" ((Adj.mem_matrix (0, 4) val1_map) == true);
  check "Get element" ((Adj.mem_matrix (0, 6) val1_map) == true);
  check "Get element" ((Adj.mem_matrix (0, 9) val1_map) == false);
  check "Get element" ((Adj.mem_matrix (7, 0) val1_map) == true);
  check "Get element" ((Adj.mem_matrix (7, 1) val1_map) == false);
  check "Get element" ((Adj.mem_matrix (4, 0) val1_map) == false);
  check "Get element" ((Adj.mem_matrix (4, 2) val1_map) == false);
  check "Get element" ((Adj.mem_matrix (3, 3) val1_map) == true);
  check "Get element" ((Adj.mem_matrix (9, 9) val1_map) == false)

let test_bounds () =
  let ((min_x, max_x), (min_y, max_y)) = (Adj.bounds val1_map) in
  check "Check bounds of map1" ( (min_x == 0) && (max_x == 7) && (min_y == 0) && (max_y == 6) )

let tests =
  [
    ("Test_Mem", `Quick, test_mem);
    ("Test_Get", `Quick, test_get);
    ("Test_Mem_Matrix", `Quick, test_mem_matrix);
    ("Test_Bounds", `Quick, test_bounds)
  ]

let test_suites : unit Alcotest.test list =
  [
    ("Robozzle", tests);
  ]

let () = Alcotest.run "proj" test_suites
