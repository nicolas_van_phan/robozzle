
open Base

(* Adjacency list *)
type 'a t = (int * 'a) list

(* Map of the level (adjacency list of adjacency list) *)
type 'a matrix = ('a t) t

(* Returns the list element at the specified index *)
let get index list =
  let elt = List.find ~f:(fun elt ->
    match elt with
    | (i, _) when i = index -> true
    | _ -> false
  ) list
  in match elt with
  | Some (_, v) -> Some v
  | None -> None

(* Checkes whether an element exists at the specified index *)
let mem index list =
  match get index list with
  | Some _ -> true
  | None -> false

(* Adds elt to the list and returns the new list  *)
let set index value list =
  let rec aux = function
    | [] -> [ (index, value) ]
    | ((i, _) as hd) :: tl ->
      if i < index then hd :: (aux tl)
      else if i = index then (index, value) :: tl
      else  (index, value) :: hd :: tl
  in aux list


let get_matrix (x, y) map =
  match get x map with
  | None -> None
  | Some row -> get y row

let mem_matrix (x, y) map =
  match get_matrix (x, y) map with
  | None -> false
  | Some _ -> true

let set_matrix (x, y) color map =
  let old_row = get x map in
  let new_row =
    match old_row with
    | None -> [ (y, color) ]
    | Some row -> set y color row
  in
  set x new_row map

let get_first_index adj_list =
  match adj_list with
  | [] -> 0
  | (i, _) :: _ -> i

let rec get_last_index adj_list =
  match adj_list with
  | [] -> 0
  | [(i, _)] -> i
  | _ :: tl -> get_last_index tl

let bounds map =
  let (min_x, first_row) = match List.hd map with
  | None -> (0, [])
  | Some elt -> elt
  in
  let max_x = get_last_index map in
  let min_y = List.fold_left ~init:(get_first_index first_row) ~f:(fun acc (_, row) ->
    let min_y_row = get_first_index row in
    if min_y_row < acc then min_y_row else acc
  ) map in
  let max_y = List.fold_left ~init:(get_last_index first_row) ~f:(fun acc (_, row) ->
    let max_y_row = get_last_index row in
    if max_y_row > acc then max_y_row else acc
  ) map in
  ((min_x, max_x), (min_y, max_y))


let () = Stdio.print_endline "Yahoo !"