
let () =
  Dream.run
  @@ Dream.logger
  @@ Dream.router [

    Dream.get "/data/**"    (Dream.static "./app/data");
    Dream.get "/"           (Dream.from_filesystem "app" "index.html");
    Dream.get "/index.html" (Dream.from_filesystem "app" "index.html");

  ]
  (* TODO : Re-enable this option *)
  (* Its input type must have changed in the latest Dream version
     since compilation fails now with a type mismatch error
     between Dream.router output and Dream.not_found input *)
  (* @@ Dream.not_found *)