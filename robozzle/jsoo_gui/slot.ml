
open! Js_of_ocaml
open! Js_of_ocaml_lwt
open Jsoo_defs
open Vm

let slot_border = "2px solid black"
let slot_border_focused = "2px dotted black"
let slot_size = "30px"

type slot = {
    cmd : Game.cmd ref;
    color : Level.color ref;
    cell : Html.tableCellElement Js.t
}

type t = {
    matrix : slot array array;
    cur_x : int ref;
    cur_y : int ref
}

(* ---------- Change currently selected slot upon click ---------- *)

let click_handler (t : t) x y _ =
  let cur_x = t.cur_x in
  let cur_y = t.cur_y in
  let old_cell = t.matrix.(!cur_x).(!cur_y).cell in
  let new_cell = t.matrix.(x).(y).cell in
  old_cell##.style##.border := Js.string slot_border;
  new_cell##.style##.border := Js.string slot_border_focused;
  cur_x := x;
  cur_y := y;
  let soi = string_of_int in
  Stdio.print_endline ("Click on cell " ^ (soi x) ^ " " ^ (soi y) ); Js._false

let get_cur_slot (t : t) =
  let {matrix = matrix ; cur_x = cur_x ; cur_y = cur_y} = t in
  matrix.(!cur_x).(!cur_y)

(* ---------- Change slot content upon key press ---------- *)

let set_slot_color slot color =
  Stdio.print_endline "Call set_slot_color";
  slot.color := color;
  let open Level in
  let new_color = match color with
    | Green  -> green
    | Blue   -> blue
    | Yellow -> yellow
    | Red    -> red
    | Empty  -> white
  in
  slot.cell##.style##.backgroundColor := new_color

let set_slot_command slot command =
  Stdio.print_endline "Call set_slot_command";
  slot.cmd := command;
  let text = match command with
    | Move Fwd   -> "^"
    | Move Left  -> "<"
    | Move Right -> ">"
    | Empty      -> ""
    | Func i     -> "F " ^ string_of_int (i + 1)
  in
  slot.cell##.textContent := Js.some (Js.string text)

let erase_all_slots (t : t) =
  Array.iter (fun slot_row ->
    Array.iter (fun slot ->
      set_slot_color slot Empty;
      set_slot_command slot Empty;
    ) slot_row
  ) t.matrix

let keydown_handler (t : t) ev =
  let cur_slot = get_cur_slot t in
  let set_color = set_slot_color cur_slot in
  let set_cmd = set_slot_command cur_slot in
  let keycode = Js.to_string (Js.Optdef.get ev##.key (fun () -> Js.string "")) in
  Stdio.print_endline ("Pressed key : " ^ keycode);
  let nb_func = Array.length t.matrix in
  let in_bounds nb = nb >= 1 && nb <= nb_func in
  let open Level in
  let () = match keycode with
    | "r"           -> set_color Red
    | "g"           -> set_color Green
    | "b"           -> set_color Blue
    | "y"           -> set_color Yellow
    | "ArrowUp"     -> set_cmd (Move Fwd)
    | "ArrowLeft"   -> set_cmd (Move Left)
    | "ArrowRight"  -> set_cmd (Move Right)
    | "Backspace"   -> set_color Empty; set_cmd Empty
    | "D"           -> erase_all_slots t
    | ("0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9") as str_nb
      when in_bounds (int_of_string str_nb) ->
          set_cmd (Func ((int_of_string str_nb) - 1))
    | _ -> ()
  in
  Lwt.return ()

(* ---------- Extract functions from slots ----------  *)

let cmd_of_slot (slot : slot) : Game.instruction =
  { cmd = !(slot.cmd) ; color = !(slot.color) }

let function_of_slot_row (slow_row : slot array) : Game.instruction list =
  Array.to_list (Array.map cmd_of_slot slow_row)

let instructions_of_slots (t : t) : Game.instruction list list =
  let { matrix = matrix; cur_x = _; cur_y = _ } = t in
  Array.to_list (Array.map function_of_slot_row matrix)

(* ---------- Create slot matrix ---------- *)

let new_slot html_row _ =
  let cell = Html.createTd doc in
  let cmd : Game.cmd ref = ref Game.Empty in
  let color = ref Level.Empty in
  cell##.style##.border := Js.string slot_border;
  cell##.style##.width  := Js.string slot_size;
  cell##.style##.height := Js.string slot_size;
  cell##.style##.textAlign := Js.string "center";
  Dom.appendChild html_row cell;
  { cmd = cmd ; color = color ; cell = cell }

let new_slot_row html_table funcs x =
  let (_, func_len) = List.nth funcs x in
  let html_row = Html.createTr doc in
  Dom.appendChild html_table html_row;
  let html_row_title = Html.createTd doc in
  html_row_title##.textContent := Js.some (Js.string ("F" ^ string_of_int (x + 1)));
  Dom.appendChild html_row html_row_title;
  Array.init func_len (new_slot html_row)

let create (level : Level.t) html_table =
  let funcs = level.functions in
  let nb_func = List.length funcs in

  (* Create array of slot rows *)
  let matrix = Array.init nb_func (new_slot_row html_table funcs) in
  let cur_x = ref 0 in
  let cur_y = ref 0 in
  let t = { matrix; cur_x; cur_y } in

  (* Attach click_handler to each cell *)
  Array.iteri (fun x slot_row ->
    Array.iteri (fun y slot ->
      let cell = slot.cell in
      cell##.onmousedown := Html.handler (click_handler t x y)
    ) slot_row
  ) matrix;
  t
