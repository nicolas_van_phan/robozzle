
open Vm
open! Js_of_ocaml

(* TODO : Find better types to represent a slot and a slot matrix,
          having both type slot and type t in a module called Slot is confusing *)
type slot
type t

val create : Level.t -> Dom_html.element Js.t -> t
val click_handler : t -> int -> int -> unit -> bool Js_of_ocaml.Js.t
val keydown_handler : t -> Dom_html.keyboardEvent Js.t -> unit Lwt.t
val instructions_of_slots : t -> Game.instruction list list