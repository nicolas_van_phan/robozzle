
open! Js_of_ocaml
open! Js_of_ocaml_lwt
open Jsoo_defs
open Vm

(* Level config *)
let tile_size = 40.
let watchdog_max_thold = 500
let sleep_duration = 0.1
let i2p i = tile_size *. float_of_int i
let level_bg_color = grey
let img_star        = "data/imgs/star.png"
let img_robot_up    = "data/imgs/robot_up.png"
let img_robot_down  = "data/imgs/robot_down.png"
let img_robot_left  = "data/imgs/robot_left.png"
let img_robot_right = "data/imgs/robot_right.png"

let draw_star ctx x y =
  let img = Html.createImg doc in
  img##.src := (Js.string img_star);
  ctx##drawImage img (i2p x) (i2p y)

let draw_tile ctx x y color =
  let open Level in
  let style = match color with
    | Blue   -> blue
    | Red    -> red
    | Green  -> green
    | Yellow -> yellow
    | Empty  -> light_grey
  in
  ctx##.fillStyle := black;
  ctx##fillRect (i2p y) (i2p x) (tile_size +. 1.) (tile_size +. 1.);
  ctx##.fillStyle := style;
  ctx##fillRect ((i2p y) +. 1.) ((i2p x) +. 1.) (tile_size -. 1.) (tile_size -. 1.);
  ()

let draw_row ctx x row =
  List.iter (fun (y, color) -> draw_tile ctx x y color) row

let draw_map ctx (level : Level.t) =
  List.iter (fun (x, row) -> draw_row ctx x row) level.grid

let draw_stars ctx (level : Level.t) =
  List.iter (fun (x, y) -> draw_star ctx x y) level.stars

(* TODO : Dont reload an img every time you re-draw the map *)
let draw_robot ctx (level : Level.t) =
  let (x, y) = level.robot.pos in
  let img = Html.createImg doc in
  let src = match level.robot.dir with
    | Up ->    (Js.string img_robot_up)
    | Down ->  (Js.string img_robot_down)
    | Left ->  (Js.string img_robot_left)
    | Right -> (Js.string img_robot_right)
  in
  img##.src := src;
  ctx##drawImage img (i2p x) (i2p y)

let draw_background ctx (level : Level.t) =
  let (w, h) = level.canvas in
  ctx##.fillStyle := level_bg_color;
  ctx##fillRect 0. 0. (i2p w) (i2p h)

let draw_level ctx (level : Level.t) =
  draw_background ctx level;
  draw_map ctx level;
  draw_stars ctx level;
  draw_robot ctx level

let create_canvas html_app (level : Level.t) =
  let (w, h) = level.canvas in
  let w = w * (int_of_float tile_size) + 20 in
  let h = h * (int_of_float tile_size) + 20 in
  let canvas = Html.createCanvas doc in
  canvas##.width := w;
  canvas##.height := h;
  Dom.appendChild html_app canvas;
  canvas##getContext Html._2d_

let get_level_path =
  let url_args = Url.Current.arguments in
  let rec get_url_arg argname args =
    match args with
    | [] -> "level1" (* Default level *)
    | (name, value) :: tl ->
      Stdio.print_endline ("name : " ^ name ^ "; value : " ^ value);
      if String.equal name argname then
        value
      else
        get_url_arg argname tl
  in
  let level_name = get_url_arg "level" url_args in
  "data/levels/" ^ level_name ^ ".json"

type run_state = Play | Pause

let set_run_state run_state new_state _ =
  Stdio.print_endline "----- Call play_pause_game -----";
  run_state := new_state;
  Lwt.return ()

let handle_play_pause run_state play_pause_button step_button =
  let set_run_state = set_run_state run_state in
  match !run_state with
    | Play -> Lwt.return ()
    | Pause ->
      (* Program the buttons to restart the game on next click, and wait for button click *)
      Stdio.print_endline "Waiting for wakeup...";
      let* _ = Lwt.pick [ Lwt_js_events.click play_pause_button >>= (set_run_state Play);
                          Lwt_js_events.click step_button       >>= (set_run_state Pause) ] in
      Stdio.print_endline "Woken up !";
      (* Reprogram the buttons to pause the game on next click *)
      Lwt.ignore_result( Lwt.pick [ Lwt_js_events.click play_pause_button >>= (set_run_state Pause);
                                    Lwt_js_events.click step_button       >>= (set_run_state Pause) ] );
      Lwt.return ()


let game_loop init_level display_level functions =

  let run_state = ref Pause in
  let play_pause_button = get_element_by_id "play_pause_button" in
  let step_button = get_element_by_id "step_button" in
  (* TODO : Initialize game in the Game module *)
  let game : Game.t = {
    program = functions;
    cmd_list = (List.nth functions 0);
    level = init_level;
    state = Ongoing;
  } in

  let rec game_loop_aux game watchdog =
    Stdio.print_endline "----------Call game_loop_aux ----------";

    (* The watchdog counts the number of recursion level and stops if it exceeds the max threshold *)
    if watchdog = 0 then Lwt.return (Stdio.print_endline "Watchdog timeout !") else

    (* Wait when the game is paused, and handle play/pause/step button clicks *)
    let* _ = handle_play_pause run_state play_pause_button step_button in

    (* Execute the game's next command *)
    let game_next = Game.run_step game in

    (* Refresh level and sleep for a lap *)
    (* TODO : Don't refresh level on empty commands *)
    let* _ =
      display_level game_next.level;
      Lwt_js.sleep sleep_duration
    in

    (* Go to next slot (or exit if game is over) *)
    match game_next.state with
    | Won  -> Stdio.print_endline "Congratulations, you WIN !!"; fst (Lwt.wait ())
    | Lost -> Lwt.return (Stdio.print_endline "You lost... Try again !")
    | Ongoing -> game_loop_aux game_next (watchdog - 1)
  in
  game_loop_aux game watchdog_max_thold


let onload _ =
  Lwt.ignore_result (
    let html_app = get_element_by_id "app" in
    let* file_content = getfile get_level_path in
    let* level = Lwt.return (Level.level_from_json_string file_content) in
    let ctx = create_canvas html_app level in

    (* Display the slots *)
    let html_commands = get_element_by_id "commands" in
    let slots : Slot.t = Slot.create level html_commands in

    let reset_button = get_element_by_id "reset_button" in
    let rec main_loop _ =
      draw_level ctx level;
      let* _ = Lwt.pick [ game_loop level (draw_level ctx) (Slot.instructions_of_slots slots);
                          Lwt_js_events.click reset_button >>= (fun _ -> Lwt.return ());
                          Lwt_js_events.keydown doc >>= (Slot.keydown_handler slots) ] in
      main_loop ()
    in
    main_loop ()
  );
  Js._false
