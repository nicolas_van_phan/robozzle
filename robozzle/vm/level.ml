
open Base
open Stdio
open Lib_adj
open Lib_json

type color = Green | Blue | Yellow | Red | Empty

type orientation = Up | Down | Left | Right

type coordinates = int * int

type move = Fwd | Left | Right

type robot_state = {
  pos : coordinates;
  dir : orientation;
}

type t = {
  grid : color Adj.matrix;
  robot : robot_state;
  functions : (string * int) list;
  stars : (int * int) list;
  canvas : (int * int);
}

(* ---------- Functions to build the Level.t from the JSON AST ---------- *)

let functions_from_ast ast =
  List.map ast ~f:(fun elt ->
    match elt with
    | `Assoc [ ("name", `String name); ("size", `Int size) ] -> (name, size)
    | _ -> printf "Json : Wrong format for robot function"; Caml.exit (-1)
    )

let robot_pos_from_ast ast =
  match ast with
  | [`Int x; `Int y] -> (x, y)
  | _ -> printf "Json : Wrong format for robot position"; Caml.exit (-1)

let robot_dir_from_ast ast =
  match ast with
  | "up" -> Up
  | "left" -> Left
  | "right" -> Right
  | "down" -> Down
  | _ -> printf "Json : Wrong value %s for robot direction" ast; Caml.exit (-1)

let stars_from_ast ast =
  List.map ast ~f:(fun elt ->
    match elt with
    | `List [ `Int x; `Int y ] -> (x, y)
    | _ -> printf "Json : Wrong format for star position"; Caml.exit (-1)
    )

let grid_row_from_ast ast =
  let explode s = List.init (String.length s) ~f:(String.get s) in
  match ast with
  | `String str ->
    List.filter_mapi (explode str) ~f:(fun i c -> match c with
    | 'r' -> Some (i, Red)
    | 'y' -> Some (i, Yellow)
    | 'g' -> Some (i, Green)
    | 'b' -> Some (i, Blue)
    | '.' -> Some (i, Empty)
    | '_' -> None
    | _ -> printf "Json : Wrong format for grid case"; Caml.exit (-1)
    )
  | _ -> printf "Json : Wrong format for grid row"; Caml.exit (-1)

let grid_from_ast ast =
  List.mapi ast ~f:(fun i elt ->
    (i, grid_row_from_ast elt)
  )

let canvas_from_ast ast =
  let h = List.length ast in
  let fst = List.hd ast in
  let w = match fst with
    | Some (`String str) -> String.length str
    | _ -> 0
  in
  (w, h)

let level_from_json_string content : t =
  let ast = Json.ast_from_string content in
  match ast with
  (* TODO : Allow definition of a level in any order  *)
  | `Assoc [ ("functions", `List functions);
             ("robot_location", `List robot_pos);
             ("robot_orientation", `String robot_dir);
             ("stars", `List stars_pos);
             ("map", `List grid)
           ] ->
    {
      grid = grid_from_ast grid;
      robot = {
        pos = robot_pos_from_ast robot_pos;
        dir = robot_dir_from_ast robot_dir };
      functions = functions_from_ast functions;
      stars = stars_from_ast stars_pos;
      canvas = canvas_from_ast grid
    }
  | _ ->
    printf "Json : Unexpected IDs in file";
    Caml.exit (-1)


(* ---------- Functions to get information on the level ----------------- *)

let won (level : t) =
  (List.length level.stars) = 0

let lost (level : t) =
  let (x, y) = level.robot.pos in
  let tile = Adj.get_matrix (y, x) level.grid in
  match tile with
  | None -> true
  | Some _ -> false

let color_match (level : t) (tgt_color : color) =
  (* TODO : Change Adj so that we don't have to always invert x and y, it's error-prone *)
  let (x, y) = level.robot.pos in
  match Adj.get_matrix (y, x) level.grid with
  | None ->
    Stdio.print_endline "Fell of the level ??";
    false (* Happens only if the robot falls off the level, in which case the game is lost anyway *)
  | Some tile_color -> match (tgt_color, tile_color) with
    (* TODO : Remove this terrible variant comparison *)
    | (Empty , _)  | (Green, Green) | (Blue , Blue)  | (Red , Red)  | (Yellow , Yellow) -> true
    | _ -> false

(* ---------- Functions to udpate the level (move robot and stars) ------ *)

let remove_star (level : t) =
  { level with stars = List.filter ~f:(fun (x, y) ->
    let (robot_x, robot_y) = level.robot.pos in ((robot_x <> x) || (robot_y <> y)) ) level.stars
  }

let pos_fwd dir (x, y) =
  match dir with
    | Up    -> (x, y - 1)
    | Down  -> (x, y + 1)
    | Left  -> (x - 1, y)
    | Right -> (x + 1, y)

(* TODO : It's weird the output type is inferred to be 'move' instead of 'orientation' *)
let dir_left dir : orientation =
  match dir with
    | Up    -> Left
    | Down  -> Right
    | Left  -> Down
    | Right -> Up

let dir_right dir : orientation =
  match dir with
    | Up    -> Right
    | Down  -> Left
    | Left  -> Up
    | Right -> Down

let move_robot (level : t) (move : move) =
  let dir = level.robot.dir in
  let pos = level.robot.pos in
  let (new_dir, new_pos) = match move with
    | Left  -> (dir_left dir,  pos)
    | Right -> (dir_right dir, pos)
    | Fwd   -> (dir,           pos_fwd dir pos)
  in
  { level with robot = { pos = new_pos; dir = new_dir } }
