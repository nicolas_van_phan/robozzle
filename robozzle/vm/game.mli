
type cmd =
  | Move of Level.move
  | Func of int
  | Empty

type state =
  | Won
  | Lost
  | Ongoing

type instruction = {
  cmd : cmd;
  color : Level.color
}

type game_function = instruction list

(* Represents an instance of the running game *)
type t = {
  program : game_function list;   (* List of functions entered by the user *)
  cmd_list : instruction list;  (* List of instructions left to execute *)
  level : Level.t;
  state : state;
}

val run_step : t -> t