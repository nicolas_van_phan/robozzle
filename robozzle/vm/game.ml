
type cmd =
  | Move of Level.move
  | Func of int
  | Empty

type state =
  | Won
  | Lost
  | Ongoing

type instruction = {
  cmd : cmd;
  color : Level.color
}

type game_function = instruction list

(* Represents an instance of the running game *)
type t = {
  program : game_function list;   (* List of functions entered by the user *)
  cmd_list : instruction list;  (* List of instructions left to execute *)
  level : Level.t;
  state : state;
}

let run_step (game : t) =
  Stdio.print_endline "Call run_step";
  match game.cmd_list with
  | [] ->
    Stdio.print_endline "!!! cmd_list is empty !!!";
    let new_state = match game.state with
    | Won -> Won
    | Lost -> Lost
    | Ongoing -> if Level.won game.level then Won else Lost (* If the robot is done but didn't get all the stars, game is lost *)
    in
    { game with state = new_state; }
  | hd :: tl ->
    Stdio.print_endline "cmd_list not empty";
    let (level, cmd_list) =
      if (Level.color_match game.level hd.color) = false then begin
        Stdio.print_endline "Color MISMATCH";
        (game.level, tl)
      end else match hd.cmd with
      | Empty  ->  (game.level, tl)
      | Move m ->  (Level.move_robot game.level m, tl)
      | Func i ->  (game.level, (List.nth game.program i) @ tl)
    in
    let level = Level.remove_star level in
    let state =
      if Level.won game.level then Won
      else if Level.lost game.level then Lost
      else Ongoing
    in
    let () = match state with
    | Won -> Stdio.print_endline "Game - WON !!!"
    | Lost -> Stdio.print_endline "Game - LOST !!!"
    | Ongoing -> Stdio.print_endline "Game - Next game iteration..."
    in
    { game with
       level = level;
       cmd_list = cmd_list;
       state = state;
    }
