
open Lib_adj

type color = Green | Blue | Yellow | Red | Empty

type orientation = Up | Down | Left | Right

type coordinates = int * int

type move = Fwd | Left | Right

type robot_state = {
  pos : coordinates;
  dir : orientation;
}

(* Represents the state of the game (map, robot, stars)*)
(* TODO : In a second step, astract this type *)
type t = {
  grid : color Adj.matrix;
  robot : robot_state;
  functions : (string * int) list;
  stars : (int * int) list;
  canvas : (int * int);
}

(* Initializes a level from a json string description of the level *)
val level_from_json_string : string -> t

(* Says if the robot is on the given color or not *)
val color_match : t -> color -> bool

val won : t -> bool

val lost : t -> bool

val remove_star : t -> t

val move_robot : t -> move -> t