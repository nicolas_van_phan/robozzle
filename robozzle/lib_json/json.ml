
open Stdio

let parse_with_error lexbuf =
  match Json_parser.prog Json_lexer.read lexbuf with
  | exception Json_lexer.SyntaxError msg ->
      (* printf "%a: %s\n" print_position lexbuf msg; *)
      printf "Syntax error : %s\n" msg;
      Caml.exit (-1)
  | exception Json_parser.Error ->
      (* printf "%a: syntax error\n" print_position lexbuf; *)
      printf "Syntax error\n";
      Caml.exit (-1)
  | None ->
      printf "File is empty";
      Caml.exit (-1)
  | Some value ->
    (* TODO : Re-enable this function using Base only and not Core_kernel *)
      (* printf "%a\n" Json_ast.output_value value; *)
      value

let ast_from_string file_content =
  let lexbuf = Lexing.from_string file_content in
  parse_with_error lexbuf
